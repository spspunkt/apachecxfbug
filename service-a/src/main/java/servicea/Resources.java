package servicea;

import java.net.MalformedURLException;
import java.sql.Date;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RestClient;

import servicea.domain.ServiceBClientView;

@Path("/res")
public class Resources {

	@Inject
	@RestClient
	private ServiceBClientView client;

	@GET
	@Path("/plaintext")
	@Produces({ MediaType.TEXT_PLAIN })
	public Response plaintext() throws MalformedURLException {
		String result = client.plainText("test it!");
		return Response.status(200).entity(result).build();
	}

	@GET
	@Path("/dtojson")
	@Produces({ MediaType.TEXT_PLAIN })
	public Response dtoJson() {
		Date result = client.dTOJson(new Date(System.currentTimeMillis()));
		return Response.status(200).entity(result.toString()).build();
	}

	@GET
	@Path("/dtojson200")
	@Produces({ MediaType.TEXT_PLAIN })
	public Response dtoJson200() {
		Date result = client.dTOJson200(new Date(System.currentTimeMillis()));
		return Response.status(200).entity(result.toString()).build();
	}
}