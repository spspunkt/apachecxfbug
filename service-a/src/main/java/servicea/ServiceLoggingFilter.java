package servicea;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class ServiceLoggingFilter implements ClientRequestFilter, ClientResponseFilter {

	// @Inject CDI-Bug? Use API instead:
	private Logger logger = LoggerFactory.getLogger(ServiceLoggingFilter.class);

	@Override
	public void filter(ClientRequestContext requestContext) throws IOException {
		logger.info(">>> request {}: {} ({}), headers: {}", requestContext.getMethod(), requestContext.getUri(),
				Boolean.valueOf(requestContext.hasEntity()), requestContext.getHeaders());

		if (logger.isDebugEnabled() && requestContext.hasEntity()
				&& isValidJSONMediaType(requestContext.getMediaType())) {
			try (Jsonb jsonb = JsonbBuilder.create()) {
				// TODO use javax.ws.rs.ext.WriterInterceptor for container serialized output
				// instead
				String result = jsonb.toJson(requestContext.getEntity());
				logger.debug(String.format(">>>   request body '%s'", result),
						logger.isTraceEnabled() ? new Exception("Trace my stack ;)") : null);
			} catch (final Exception e) {
				logger.error("Debug logging failed", e);
			}
		}
	}

	@Override
	public void filter(ClientRequestContext requestContext, ClientResponseContext responseContext) throws IOException {
		logger.info("<<< response {}: {}, status: {} ({}), headers: {}", requestContext.getMethod(),
				requestContext.getUri(), Integer.valueOf(responseContext.getStatus()),
				Boolean.valueOf(requestContext.hasEntity()), responseContext.getHeaders());

		if (logResonseEntity() && logger.isDebugEnabled() && responseContext.hasEntity()) {
			InputStream entityStream = responseContext.getEntityStream();
			if (entityStream.available() > 0) {
				byte[] bodyBytes = readInputStreamBytes(entityStream);
				responseContext.setEntityStream(new ByteArrayInputStream(bodyBytes));
				logger.debug("<<<   response body ({}) '{}'", entityStream.toString(),
						new String(bodyBytes, Charset.forName("UTF-8")));// TODO RspCharset
			}
		}
	}

	private byte[] readInputStreamBytes(InputStream entityStream) throws IOException {
		final ByteArrayOutputStream result = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int length;
		while ((length = entityStream.read(buffer)) != -1) {
			result.write(buffer, 0, length);
		}
		return result.toByteArray();
	}

	private boolean isValidJSONMediaType(MediaType mediaType) {
		return mediaType != null && (mediaType.toString().contains(MediaType.APPLICATION_JSON));
	}

	private boolean logResonseEntity() {
		return true;
	}
}