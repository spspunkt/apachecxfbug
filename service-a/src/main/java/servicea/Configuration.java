package servicea;

public class Configuration {

	// http://localhost:8080/service-a/res -> 200 "Ok"
	public final static String SERVICEA_HOST = "http://localhost:8080";
	public final static String SERVICEA_WEBCONTEXT = "service-a";

	// http://localhost:8080/service-b/res -> 200 "Ok"
	public final static String SERVICEB_HOST = "http://localhost:8080";
	public final static String SERVICEB_WEBCONTEXT = "service-b";

}