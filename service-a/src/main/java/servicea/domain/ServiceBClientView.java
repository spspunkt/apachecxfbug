package servicea.domain;

import java.sql.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.eclipse.microprofile.rest.client.annotation.RegisterProvider;
import org.eclipse.microprofile.rest.client.annotation.RegisterProviders;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import servicea.Configuration;
import servicea.ServiceLoggingFilter;

/**
 * 
 * Service B accessor
 *
 */
@RegisterRestClient(baseUri = Configuration.SERVICEB_HOST + "/" + Configuration.SERVICEB_WEBCONTEXT)
@Path("/res")
@RegisterProviders(value = {
		@RegisterProvider(value = ServiceLoggingFilter.class) })
public interface ServiceBClientView {

    @POST
	@Path("/plaintext")
	@Consumes({ "text/plain" })
	@Produces({ "text/plain" })
	String plainText(String text);
    
    @POST
	@Path("/dtojson")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
	Date dTOJson(Date date);

	@POST
	@Path("/dtojson200")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	Date dTOJson200(Date date);
}
