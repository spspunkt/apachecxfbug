package it;

import org.junit.BeforeClass;
import org.junit.Test;

import io.restassured.RestAssured;
import servicea.Configuration;

public class TestIT {

	@BeforeClass
	public static void init() {
		RestAssured.baseURI = Configuration.SERVICEA_HOST;
		RestAssured.basePath = Configuration.SERVICEA_WEBCONTEXT + "/res";
	}

	@Test
	public void plainText() {
		RestAssured.get("/plaintext").then().assertThat().statusCode(200);
	}

	@Test
	public void dtoJson200() {
		RestAssured.get("/dtojson200").then().assertThat().statusCode(200);
	}

	@Test
	public void dtoJson() {
		RestAssured.get("/dtojson").then().assertThat().statusCode(200);
	}
}