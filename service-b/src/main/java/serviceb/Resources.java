package serviceb;

import java.sql.Date;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/res")
public class Resources {

	@GET
	@Produces({ MediaType.TEXT_PLAIN })
	public Response test() {
		return Response.status(202).entity("Ok").build();
	}

	@POST
	@Path("/plaintext")
	@Produces({ MediaType.TEXT_PLAIN })
	@Consumes({ MediaType.TEXT_PLAIN })
	public Response plainText(String text) {
		return Response.status(202).entity(UUID.randomUUID().toString() + ": " + text).build();
	}

	@POST
	@Path("/dtojson")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response dtoJson(Date date) {
		return Response.status(202).entity(new Date(date.getTime())).build();
	}

	@POST
	@Path("/dtojson200")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response plainText200(Date date) {
		return Response.status(200).entity(new Date(date.getTime())).build();
	}
}