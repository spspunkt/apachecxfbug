# Problemstellung:
leere JAX-RS Entity der WLP-MP-RestClient-Impl nach einem POST-Request mit Status-Code 202 und Loggingfilter.

## service-a
Frontend Service der mittels MP-RestClient service-b mittels POST-Request aufruft.

## service-b
Backend Service der einen POST-Request mit einem SC202 beantwortet.

## Teststellung/Fehler im  WLP 19.0.0.6/wlp-1.0.29.cl190620190617-1530 (aber auch OLP 19.0.0.10)

### service-a: it.TestIT.dtoJson NOK
[ERROR   ] SRVE0315E: An exception occurred: java.lang.Throwable: javax.ws.rs.client.ResponseProcessingException: Problem with reading the data, class java.sql.Date, ContentType: application/json.
	at com.ibm.ws.webcontainer.webapp.WebApp.handleRequest(WebApp.java:5033)
	at [internal classes]
Caused by: javax.ws.rs.client.ResponseProcessingException: Problem with reading the data, class java.sql.Date, ContentType: application/json.
	at org.apache.cxf.jaxrs.impl.ResponseImpl.reportMessageHandlerProblem(ResponseImpl.java:527)
	at [internal classes]
	at com.sun.proxy.$Proxy87.dTOJson(Unknown Source)
	at servicea.Resources.dtoJson(Resources.java:36)
	at sun.reflect.GeneratedMethodAccessor857.invoke(Unknown Source)
	at java.lang.reflect.Method.invoke(Unknown Source)
	at com.ibm.ws.jaxrs20.cdi.component.JaxRsFactoryImplicitBeanCDICustomizer.serviceInvoke(JaxRsFactoryImplicitBeanCDICustomizer.java:302)
	... 1 more
Caused by: javax.json.bind.JsonbException: Internal error: null
	at org.eclipse.yasson.internal.Unmarshaller.deserializeItem(Unmarshaller.java:76)
	... 7 more
Caused by: java.util.NoSuchElementException
	at org.glassfish.json.JsonParserImpl.next(JsonParserImpl.java:361)
	... 7 more
	
#### Analyse
servicea.ServiceLoggingFilter.filter(ClientRequestContext, ClientResponseContext) wird doppelt durchlaufen, was aber ggf. Spec-Konform erscheint. ("")

Beim zweiten Durchlaufen ist die Entity leer (bzw. liefert InoutStream#read eine -1)

### service-a: it.TestIT.dtoJson200 OK
hier liefert service-b auch eine POST-Entity, aber mittels SC200

### service-a: it.TestIT.plainText OK
hier liefert service-b eine Plain-Text POST-Body, mittels SC202

### sobald mittels servicea.ServiceLoggingFilter.logResonseEntity() das Entity-Logging unterdr�ckt wird ist it.TestIT.dtoJson OK!

## Verprobung Wildfly 18

### service-a: it.TestIT.dtoJson OK
### service-a: it.TestIT.dtoJson200 OK
### service-a: it.TestIT.plainText OK


 